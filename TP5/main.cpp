#include <iostream>
#include <string.h>
#include <chrono>
#include <random>
#include <fstream>
#include <unistd.h>

#include "table.h"

using namespace std;

int hachage(const Cle c, int taille){			//On hache juste la cle en fonction de la taille
	return c % taille;
}

int rehachageLineaire(Cle index, int essais, int taille){	//On essaie la case + 1 
	return (index + 1 + (essais - essais)) % taille;		//(essais - essais) = pas de Warning (op strat')
}

int rehachageQuadratique(Cle index, int essais, int taille){	//On utilise un pas correspondant au nombre d'essais au carré
	return (index + (essais)*(essais)) % (taille);
}

int doubleHachage(Cle index, int essais, int taille){			//On hache en utilisant les deux fonctions de hachage précédentes 
	return (index + 
			((rehachageLineaire(index, essais, taille) + 
			rehachageQuadratique(index, essais, taille)) * (essais - 1))) % taille;
}

int main(){
	srand(time(NULL));
	ofstream monFluxLineaire("performanceLineaire.txt");
	monFluxLineaire << "# \"nb_element\" \"Temps\""<<endl;

	ofstream monFluxQuadra("performanceQuadra.txt");
	monFluxQuadra << "# \"nb_element\" \"Temps\""<<endl;

	ofstream monFluxDouble("performanceDouble.txt");
	monFluxDouble << "# \"nb_element\" \"Temps\""<<endl;

	std::chrono::time_point<std::chrono::system_clock> start, end;
	int elapsed_microseconds;

	int valeurRandom;

	Table testTempsQuadra1 = Table(&hachage, &rehachageQuadratique, 1000);
	Table testTempsQuadra2 = Table(&hachage, &rehachageQuadratique, 2000);
	Table testTempsQuadra3 = Table(&hachage, &rehachageQuadratique, 3000);
	Table testTempsQuadra4 = Table(&hachage, &rehachageQuadratique, 4000);
	Table testTempsQuadra5 = Table(&hachage, &rehachageQuadratique, 5000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 1000 ; i++){
		valeurRandom = ((rand()));
		testTempsQuadra1.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxQuadra<<"1000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);
	
	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 2000 ; i++){
		valeurRandom = ((rand()));
		testTempsQuadra2.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxQuadra<<"2000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 3000 ; i++){
		valeurRandom = ((rand()));
		testTempsQuadra3.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxQuadra<<"3000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 4000 ; i++){
		valeurRandom = ((rand()));
		testTempsQuadra4.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxQuadra<<"4000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 5000 ; i++){
		valeurRandom = ((rand()));
		testTempsQuadra5.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxQuadra<<"5000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	monFluxQuadra.close();


	Table testTempsLineaire1 = Table(&hachage, &rehachageLineaire, 1000);
	Table testTempsLineaire2 = Table(&hachage, &rehachageLineaire, 2000);
	Table testTempsLineaire3 = Table(&hachage, &rehachageLineaire, 3000);
	Table testTempsLineaire4 = Table(&hachage, &rehachageLineaire, 4000);
	Table testTempsLineaire5 = Table(&hachage, &rehachageLineaire, 5000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 1000 ; i++){
		valeurRandom = ((rand()));
		testTempsLineaire1.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxLineaire<<"1000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 2000 ; i++){
		valeurRandom = ((rand()));
		testTempsLineaire2.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxLineaire<<"2000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 3000 ; i++){
		valeurRandom = ((rand()));
		testTempsLineaire3.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxLineaire<<"3000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 4000 ; i++){
		valeurRandom = ((rand()));
		testTempsLineaire4.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxLineaire<<"4000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 5000 ; i++){
		valeurRandom = ((rand()));
		testTempsLineaire5.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxLineaire<<"5000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	
	monFluxLineaire.close();
	

	Table testTempsDouble1 = Table(&hachage, &doubleHachage, 1000);
	Table testTempsDouble2 = Table(&hachage, &doubleHachage, 2000);
	Table testTempsDouble3 = Table(&hachage, &doubleHachage, 3000);
	Table testTempsDouble4 = Table(&hachage, &doubleHachage, 4000);
	Table testTempsDouble5 = Table(&hachage, &doubleHachage, 5000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 1000 ; i++){
		valeurRandom = ((rand()));
		testTempsDouble1.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxDouble<<"1000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 2000 ; i++){
		valeurRandom = ((rand()));
		testTempsDouble2.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxDouble<<"2000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 3000 ; i++){
		valeurRandom = ((rand()));
		testTempsDouble3.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxDouble<<"3000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 4000 ; i++){
		valeurRandom = ((rand()));
		testTempsDouble4.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxDouble<<"4000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	start = std::chrono::system_clock::now();
	for(int i = 1 ; i <= 5000 ; i++){
		valeurRandom = ((rand()));
		testTempsDouble5.insere(valeurRandom, i);
	}
	end = std::chrono::system_clock::now();
	elapsed_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	monFluxDouble<<"5000"<<" "<<elapsed_microseconds<<endl;

	usleep(1000);

	monFluxDouble.close();


//Printf le elapsed_microseconds et divise par le nombre d'itérations (la 5) si tu veux le temps moyen par opération



	/*



    int choixHachage;
    Table *t;

    do{
        cout << "Quel hashage voulez-vous utiliser ?" << endl;
        cout << "Tapez 1 pour linéaire, 2 pour quadratique ou 3 pour faire un double hachage !" << endl;
        cin >> choixHachage;
    }
    while(choixHachage != 1 && choixHachage != 2 && choixHachage != 3);

    int tailleTableau;

    do{
        cout << "Quelle est la taille du tableau ?" << endl;
        cin >> tailleTableau;
    }while(tailleTableau <= 0);

    switch(choixHachage){
        case 1:
            t = new Table(&hachage, &rehachageLineaire, tailleTableau);
            break;
        case 2:
            t = new Table(&hachage, &rehachageQuadratique, tailleTableau);
            break;
        case 3:
            t = new Table(&hachage, &doubleHachage, tailleTableau);
            break;

        default:
            break;
    }

    int i = 0;
    Cle a;
    Info b;

    //Table t = Table(&hachage, &rehachageQuadratique, 5);

    while(i != t->taille){
        cout << "Quelle clef voulez-vous insérer ?" << endl;
        cin >> a;

        cout << "Quelle info voulez-vous insérer avec cette clef ?" << endl;
        cin >> b;

        t->insere(a, b);
        t->affiche();

        i++;
    }


 	*/





	
	/*
	int choixHachage;
	Table t;

	do{
		cout << "Quel hashage voulez-vous utiliser ?" << endl;
		cout << "Tapez 1 pour linéaire, 2 pour quadratique ou 3 pour faire un double hachage !" << endl;
		cin >> choixHachage;
	}
	while(choixHachage != 1 && choixHachage != 2 && choixHachage != 3);

	int tailleTableau;

	do{
		cout << "Quelle est la taille du tableau ?" << endl;
		cin >> tailleTableau;
	}while(tailleTableau < 0);


	switch(choixHachage){
		case 1:
			t = Table(&hachage, &rehachageLineaire, tailleTableau);
			break;
		case 2:
			t = Table(&hachage, &rehachageQuadratique, tailleTableau);
			break;
		case 3:
			t = Table(&hachage, &doubleHachage, tailleTableau);
	}


	
	int i = 0;
	Cle a;
	Info b;

	Table t = Table(&hachage, &rehachageQuadratique, 5);

	while(i != tailleTableau i != t.taille){
		cout << "Quelle clef voulez-vous insérer ?" << endl;
		cin >> a;

		cout << "Quelle info voulez-vous insérer avec cette clef ?" << endl;
		cin >> b;

		t.insere(a, b);
		t.affiche();

		i++;
	}


	t.insere(12,22);
	t.affiche();
	cout<<endl<<endl;
	t.insere (51,35);
	t.affiche();
	cout<<endl<<endl;
	t.insere (101,29);
	t.affiche();

*/


	return 0;
}
