#ifndef _TABLE_H
#define _TABLE_H

#include "cle.h"
#include "info.h"

class Table  {

public:

	/* Déclaration des membres */
	
	int taille;

	Cle *tabC;
	Info *tabI;
	int *occupe;

	int (*fh)(Cle c, int taille);
	int (*rh)(Cle c, int essais, int taille);
	

	/* Déclaration du constructeur */

	Table();
	Table(int (*fhache)(Cle, int), int (*rehache)(Cle, int, int), int taille);
	~Table();


	/* Fonctions */

	void insere(const Cle c, const Info i);

	Cle rechercheCle(int index);

	int rechercheIndex(const Cle c);

	Info rechercheInfo(int index);

	void suppression(const Cle c);

	void affiche();

};





#endif
