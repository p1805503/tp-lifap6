
#include <iostream>
#include "math.h"
#include "table.h"

using namespace std;


Table::Table(){

    taille = 1;
	tabC = new Cle [taille];
	tabI = new Info [taille];
	occupe = new int [taille];

/*	for(int i = 0 ; i < taille ; i++){
		tabC[i] = 0;
		tabI[i] = 0;
		occupe[i] = 0;
	}*/

}

Table::Table(int (*hache)(Cle, int), int (*rehache)(Cle, int, int), int m){

	taille = m;

	fh = hache;
	rh = rehache;

	tabC = new Cle [taille];
	tabI = new Info [taille];
	occupe = new int [taille];
}


Table::~Table(){
	delete[] tabC;
	delete[] tabI;
	delete[] occupe;
}

void Table::insere(const Cle c, const Info i){
	int index = fh(c, taille);
	int essais = 0;

	while(essais <= taille){
		if(occupe[index] == 0){
			occupe[index] = 1;
			tabC[index] = c;
			tabI[index] = i; 
			essais = taille + 1 ;
		}
		else{
			index = rh(c, essais, taille);
			essais++;
			//cout << index << " après hachage " << endl;
		}
	}
}

/*

void Table::insere(const Cle c, const Info i){

	int index = fh(c, taille), essais = 0;

	int pos = index;

	while(occupe[index] == 1){
		essais++;
		index = rh(c, essais, taille);
		if(pos == index) break;
		cout << index << " après hachage " << endl;
	}

	occupe[index] = 1;
	tabC[index] = c;
	tabI[index] = i; 

}

*/

int Table::rechercheIndex(const Cle c){
	int i = 0;
	while(tabC[i] != c){
		i++;
	}
	return i;
}


Cle Table::rechercheCle(int index){

	int a = index;
	while(occupe[index] == 0){
		cout << "aucune cle" <<endl;
		cin >> a; 
	}
	return tabC[a];
}

Info Table::rechercheInfo(int index){
	return tabI[index];

}

void Table::suppression(const Cle c){
	int index = rechercheIndex(c);
	tabI[index] = 0;
	tabC[index] = 0;
	occupe[index] = 0;
}

void Table::affiche(){

	cout << "tableau des cles "<<endl;
	for(int i = 0 ; i < taille ; i++){
		cout << tabC[i] << "  ";
	}

	cout << endl;

	cout << "tableau des infos " << endl;
	for(int j = 0 ; j < taille ; j++){
		cout << tabI[j] << "  ";
	}

	cout << endl;

	cout<<"tableau des occupation "<<endl;
	for(int k = 0 ; k < taille ; k++){
		cout << occupe[k] << "  ";
	}

	cout << endl;

}


